<?php

$mapel = ['ipa','ips','matematika'];
$kelas1 = [[
    'nama'         => 'izul',
    'kelas'        => 'TI-1C',
    'jenis_kelamin'=> 'L',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,87,85,80],'matematika' => [85,86,85,80]]
],
[
    'nama'         => 'meki',
    'kelas'        => 'TI-1C',
    'jenis_kelamin'=> 'P',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,87,85,80],'matematika' => [85,86,85,80]]

],
];
$kelas2 = [[
    'nama'         => 'aqil',
    'kelas'        => 'TI-2C',
    'jenis_kelamin'=> 'L',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,80,85,80],'matematika' => [85,80,85,80]]
],
[
    'nama'         => 'mega',
    'kelas'        => 'TI-2C',
    'jenis_kelamin'=> 'P',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,80,85,80],'matematika' => [85,80,85,80]]
],

];
$kelas3 = [[
    'nama'         => 'rifkie',
    'kelas'        => 'TI-3C',
    'jenis_kelamin'=> 'L',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [89,85,80,85],'ips' => [89,90,85,80],'matematika' => [95,80,85,90]]
],
[
    'nama'         => 'dhea',
    'kelas'        => 'TI-3C',
    'jenis_kelamin'=> 'P',
    'nilai'        => ['ipa' => [90,85,80,85],'ips' => [85,88,85,90],'matematika' => [85,89,85,90]]
],

];
$kelas4 = [[
    'nama'         => 'dimas',
    'kelas'        => 'TI-4C',
    'jenis_kelamin'=> 'L',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,80,85,80],'matematika' => [85,80,85,80]]
],
[
    'nama'         => 'sela',
    'kelas'        => 'TI-4C',
    'jenis_kelamin'=> 'P',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,80,85,80],'matematika' => [85,80,85,80]]
],

];
$kelas5 = [[
    'nama'         => 'baga',
    'kelas'        => 'TI-5C',
    'jenis_kelamin'=> 'L',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,80,85,80],'matematika' => [85,80,85,80]]
],
[
    'nama'         => 'luluk',
    'kelas'        => 'TI-5C',
    'jenis_kelamin'=> 'P',
    'mata_pelajaran'=> 'Matematika,ipa,ips',
    'nilai'        => ['ipa' => [80,85,80,85],'ips' => [85,80,85,80],'matematika' => [85,80,85,80]]
],

];




foreach ($kelas1 as $kelass1){
    $average = ['ipa','ips','matematika'];
    $nilai = [];
    $rata = 0;
    $nilai['matematika'] = implode(',',$kelass1['nilai']['matematika']);
    $nilai['ipa'] = implode(',',$kelass1['nilai']['ipa']);
    $nilai['ips'] = implode(',',$kelass1['nilai']['ips']);
    for ($i=0; $i < 3; $i++) {
        $akhir = 0;
        $angka=0; 
        $m = $mapel[$i];
        for($k=0;$k <4;$k++){
        $angka = $kelass1['nilai'][$m][$k];
        $akhir += $angka;
        }
        //$akhir[$m] += $nilai;
        $average[$m] = $akhir / 4;
        $rata += $average[$m] /3;
    }
    //$average = $akhir / 3;
    echo "Nama:".$kelass1['nama']."<br/>";
    echo "Kelas:".$kelass1['kelas']."<br/>";
    echo "Jenis Kelamin:".$kelass1['jenis_kelamin']."<br/>";
    echo "Mata pelajaran:".$kelass1['mata_pelajaran']."<br/>";
    echo "Nilai Matematika :". $nilai['matematika']."<br>";
    echo "Nilai Ipa :". $nilai['ipa']."<br>";
    echo "Nilai Ips :". $nilai['ips']."<br>";
    echo "Rata - Rata Matematika:".$average['matematika']."<br/>";
    echo "Rata - Rata ipa:".$average['ipa']."<br/>";
    echo "Rata - Rata ips:".$average['ips']."<br/>";
    echo "Rata - Rata nilai:".$rata."<br/>";
    echo "<hr>";
}

foreach ($kelas2 as $kelass2){
    $average = ['ipa','ips','matematika'];
    $nilai = [];
    $nilai['matematika'] = implode(',',$kelass2['nilai']['matematika']);
    $nilai['ipa'] = implode(',',$kelass2['nilai']['ipa']);
    $nilai['ips'] = implode(',',$kelass2['nilai']['ips']);
    for ($i=0; $i < 3; $i++) {
        $akhir = 0;
        $angka=0; 
        $m = $mapel[$i];
        for($k=0;$k <4;$k++){
        $angka = $kelass2['nilai'][$m][$k];
        $akhir += $angka;
        }
        $average[$m] = $akhir / 4;
    }
    echo "Nama:".$kelass2['nama']."<br/>";
    echo "Kelas:".$kelass2['kelas']."<br/>";
    echo "Jenis Kelamin:".$kelass2['jenis_kelamin']."<br/>";
    echo "Mata pelajaran:".$kelass2['mata_pelajaran']."<br/>";
    echo "Nilai Matematika :". $nilai['matematika']."<br>";
    echo "Nilai Ipa :". $nilai['ipa']."<br>";
    echo "Nilai Ips :". $nilai['ips']."<br>";
    echo "Rata - Rata Matematika:".$average['matematika']."<br/>";
    echo "Rata - Rata ipa:".$average['ipa']."<br/>";
    echo "Rata - Rata ips:".$average['ips']."<br/>";
    echo "<hr>";
}

// foreach ($kelas3 as $kelass3){
//     echo "Nama:".$kelass3['nama']."<br/>";
//     echo "Kelas:".$kelass3['kelas']."<br/>";
//     echo "Jenis Kelamin:".$kelass3['jenis_kelamin']."<br/>";
//     echo "Mata pelajaran:".$kelass1['mata_pelajaran']."<br/>";
//     echo "Nilai:".$kelass3['nilai']."<br/>";
//     echo "<hr>";
// }

// foreach ($kelas4 as $kelass4){
//     echo "Nama:".$kelass4['nama']."<br/>";
//     echo "Kelas:".$kelass4['kelas']."<br/>";
//     echo "Jenis Kelamin:".$kelass4['jenis_kelamin']."<br/>";
//     echo "Mata pelajaran:".$kelass1['mata_pelajaran']."<br/>";
//     echo "Nilai:".$kelass4['nilai']."<br/>";
//     echo "<hr>";
// }

// foreach ($kelas5 as $kelass5){
//     echo "Nama:".$kelass5['nama']."<br/>";
//     echo "Kelas:".$kelass5['kelas']."<br/>";
//     echo "Jenis Kelamin:".$kelass5['jenis_kelamin']."<br/>";
//     echo "Mata pelajaran:".$kelass1['mata_pelajaran']."<br/>";
//     echo "Nilai:".$kelass5['nilai']."<br/>";
//     echo "<hr>";
// }


    

          









?>