<?php
class car {
  public $name;
  public $color;

  function __construct() {
    $this->name;
  }
  function get_name() {
    $data = [];
    $data['mobilSport'][0] = [
        'name'=>'ferarri',
        'warna'=>'red',
        'type'=>'sport'];
    $data['mobilSport'][1] = [
            'name'=>'Lamborghini',
            'warna'=>'red',
            'type'=>'sport'];
    $data['mobilSuv'][0] = [
        'name'=>'Land Rover',
        'warna'=>'green',
        'type'=>'SUV'];
    $data['mobilSuv'][1] = [
        'name'=>'HRV',
        'warna'=>'white',
        'type'=>'SUV'];
    return $this->name=$data;
    
  }
}

$cars = new car();
$html = '';
$data = $cars->get_name();
foreach($data['mobilSport'] as $datas){
    $html .= 'Nama :'.$datas['name'] .'<br/>';
    $html .= 'Warna:'.$datas['warna'].'<br/>';
    $html .= 'Tipe :'.$datas['type'] .'<br/>';
}

foreach($data['mobilSuv'] as $datas2){
    $html .= 'Nama :'.$datas2['name'] .'<br/>';
    $html .= 'Warna:'.$datas2['warna'].'<br/>';
    $html .= 'Tipe :'.$datas2['type'] .'<br/>';
    }

// echo $html;

// inheritence
// class Fruit {
//     public $name;
//     public $color;
//     public function __construct($name, $color) {
//       $this->name = $name;
//       $this->color = $color;
//     }
//     public function intro() {
//       echo "The fruit is {$this->name} and the color is {$this->color}.";
//     }
//   }
  
//   class Strawberry extends Fruit {
//     public $cost;
//     public $weight;
    // public function __construct($name, $color, $cost, $weight) {
//       $this->name = $name;
//       $this->color = $color;
//       $this->cost = $cost;
//       $this->weight = $weight;
      
//     }
//     public function intro() {
//       echo "The fruit is {$this->name}, the color is {$this->color}, and the cost is {$this->cost} rupiah, per {$this->weight} gram";
//     }
//   }
  
  // $strawberry = new Strawberry("Strawberry", "red", 5000, 50 );
  // $strawberry->intro();

// Abstract class
// abstract class Car {
//     public $name;
//     public function __construct($name) {
//       $this->name = $name;
//     }
//     abstract public function intro() : string;
//   }
  
//   // Child classes
//   class Audi extends Car {
//     public function intro() : string {
//       return "Choose German quality! I'm an $this->name!";
//     }
//   }
  
//   class Volvo extends Car {
//     public function intro() : string {
//       return "Proud to be Swedish! I'm a $this->name!";
//     }
//   }
  
//   class Citroen extends Car {
//     public function intro() : string {
//       return "French extravagance! I'm a $this->name!";
//     }
//   }

//   class Ferarri extends Car {
//     public function intro() : string {
//         return "Italy best sports car i'm a $this->name!";
//     }
//   }
  
//   // Create objects from the child classes
//   $audi = new audi("Audi");
//   echo $audi->intro();
//   echo "<br>";
  
//   $volvo = new volvo("Volvo");
//   echo $volvo->intro();
//   echo "<br>";
  
//   $citroen = new citroen("Citroen");
//   echo $citroen->intro();
//   echo "<br>";

//   $Ferarri = new ferarri("Ferarri");
//   echo $Ferarri->intro();
  

  // Interface definition
// interface Animal {
//     public function makeSound();
//   }
  
//   // Class definitions
//   class Cat implements Animal {
//     public function makeSound() {
//       echo "Cat says : Meows ";
//       echo "<br>";
//     }
//   }
  
//   class Dog implements Animal {
//     public function makeSound() {
//       echo "Dog says : Barks ";
//       echo "<br>";
//     }
//   }
  
//   class Mouse implements Animal {
//     public function makeSound() {
//       echo "Mouse says : Squeaks ";
//       echo "<br>";
//     }
//   }

//   class Lion implements Animal {
//     public function makeSound(){
//         echo "Lion says : ROARR !!";
//     }
//   }
  
//   // Create a list of animals
//   $cat = new Cat();
//   $dog = new Dog();
//   $mouse = new Mouse();
//   $lion = new Lion();
//   $animals = array($cat, $dog, $mouse, $lion);
  
//   // Tell the animals to make a sound
//   foreach($animals as $animal) {
//     $animal->makeSound();
//   }
?>