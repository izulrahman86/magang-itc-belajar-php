<?php

$umur = 17;
$menikah = true;

if($umur > 18){
    if($menikah){
        echo "Selamat datang pak!";
    } else {
        echo "Selamat datang wahai pemuda!";
    }
} else {    
    echo "Maaf website ini hanya untuk yang sudah berumur 18+";
}



$level = 3;

switch($level){
    case 1:
        echo "<br> Pelajari HTML";
        break;
    case 2:
        echo "<br> Pelajari CSS";
        break;
    case 3:
        echo "<br> Pelajari Javascript";
        break;
    case 4:
        echo "<br> Pelajari PHP";
        break;
    default:
        echo "<br> Kamu bukan programmer!";
}

$books = [
    "Panduan Belajar PHP untuk Pemula",
    "Membangun Aplikasi Web dengan PHP",
    "Tutorial PHP dan MySQL",
    "Membuat Chat Bot dengan PHP"
];

echo "<h5>Judul Buku PHP:</h5>";
echo "<ul>";
foreach($books as $buku){
    echo "<li>$buku</li>";
}
echo "</ul>";