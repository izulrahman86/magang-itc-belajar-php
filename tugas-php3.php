<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Fungsi menampilkan nama gudang</h2>


  <?php
  function data($nama,$material){
    if($nama == 'operasional' || $nama == 'all'){

  
  $gudang1 = [[
    'nama'         => 'operasional',
    'kategori'        => ["raw","import","export"],
    'material' =>  ["Tanah Urug Pilihan",
    "Sirtu",
      "Pasir Teras",
      "Pasir Pasang Kali",
      "Pasir Beton",
     
  ]
  ]];
}else{
  $gudang1=[];
}

if($nama == 'perlengkapan' || $nama == 'all'){



  $gudang2 = [[
    'nama'         => 'perlengkapan',
    'kategori'        => ["raw","import","export"],
    'material' =>  ["Batu Pinggir Beton 10 x 20 x 35",
    "Batu Pinggir Beton 15 x 35 x 50 ( K-225 )",
 "Batu Telor",
 "Batako Kecil 8 x 10 x 20",
 "Batako Besar 8 x 20 x 30",
    ]]];
  }else{
    $gudang2=[];
  }

  if($nama == 'pembrangkatan' || $nama == 'all'){


  
    $gudang3 = [[
      'nama'         => 'pembrangkatan',
      'kategori'        => ["raw","import","export"],
      'material' => [ "Batu Pinggir Beton 10 x 20 x 35",
      "Batu Pinggir Beton 15 x 35 x 50 ( K-225 )",
   "Batu Telor",
   "Batako Kecil 8 x 10 x 20",
   "Batako Besar 8 x 20 x 30",
      ]]];
  }else{
    $gudang3=[];
  }

  if($nama == 'musiman' || $nama == 'all'){


  
      $gudang4 = [[
        'nama'         => 'musiman',
        'kategori'        => ["raw","import","export"],
        'material' =>  ["Paving Blok Natural 6 cm",
        "Paving Blok Warna 6 cm",
    "Canstin Paving Blok",
   "Semen PC /50 kg",
   "Semen Putih",
       ]]];
      }else{
        $gudang4=[];
      }

      if($nama == 'distribusi' || $nama == 'all'){
        $gudang5 = [[
          'nama'         => 'distribusi',
          'kategori'        => ["raw","import","export"],
          'material' => [ "Paving Blok Natural 6 cm",
          "Paving Blok Warna 6 cm",
     "Canstin Paving Blok",
     "Semen PC /50 kg",
     "Semen Putih",
        ]]];
        }else{
          $gudang5=[];
        }
         
     
  $html ='<table>
  <tr>
    <th>no</th>
    <th>nama gudang</th>
    <th>nama material</th>
    <th>kategori</th>
    <th>kode</th>
    <th>tanggal produksi</th>
    <th>harga satuan</th>
    <th>stock</th>
  </tr><tbody>';
  $no = 1;
  foreach($gudang1 as $gudangs1){
    foreach($gudangs1['kategori'] as $kat){
      $index = 1;
      $indexStock = 5;
  foreach($gudangs1['material'] as $mat){
    if($mat == $material || $material == 'all' ){ 
    $tanggalProduksi = date('d F Y',strtotime(date('Y-m-d').' + '.$index.' days '));
    $harga = 5000 + $index;
    $html .= '<tr>
    <td>'.$no.'</td>
    <td>'.$gudangs1['nama'].'</td>
    <td>'.$mat.'</td>
    <td>'.$kat.'</td>
    <td>kode-'.$index.'-'.$kat.'</td>
    <td>'.$tanggalProduksi.'</td>
    <td>'.$harga.'</td>
    <td>'.$indexStock.'</td>
    </tr>';
    $indexStock+=5;
    $no++;
    $index++;
  }
}
}
}
$html.='</tbody></table>';

$html2 ='<table>
  <tr>
    <th>no</th>
    <th>nama gudang</th>
    <th>nama material</th>
    <th>kategori</th>
    <th>kode</th>
    <th>tanggal produksi</th>
    <th>harga satuan</th>
    <th>stock</th>
  </tr><tbody>';
foreach($gudang2 as $gudangs2){
  foreach($gudangs2['kategori'] as $kat){
    $index = 1;
    $indexStock = 5;
foreach($gudangs2['material'] as $mat){
  if($mat == $material || $material == 'all' ){
  $tanggalProduksi = date('d F Y',strtotime(date('Y-m-d').' + '.$index.' days '));
    $harga = 5000 + $index;
    $html2 .= '<tr>
    <td>'.$no.'</td>
    <td>'.$gudangs2['nama'].'</td>
    <td>'.$mat.'</td>
    <td>'.$kat.'</td>
    <td>kode-'.$index.'-'.$kat.'</td>
    <td>'.$tanggalProduksi.'</td>
    <td>'.$harga.'</td>
    <td>'.$indexStock.'</td>
    </tr>';
    $indexStock+=5;
    $no++;
    $index++;
}
}
}
}
$html2.='</tbody></table>';

$html3 ='<table>
  <tr>
    <th>no</th>
    <th>nama gudang</th>
    <th>nama material</th>
    <th>kategori</th>
    <th>kode</th>
    <th>tanggal produksi</th>
    <th>harga satuan</th>
    <th>stock</th>
  </tr><tbody>';
foreach($gudang3 as $gudangs3){
  foreach($gudangs3['kategori'] as $kat){
    $index = 1;
    $indexStock = 5;
foreach($gudangs3['material'] as $mat){
  if($mat == $material || $material == 'all' ){
  $tanggalProduksi = date('d F Y',strtotime(date('Y-m-d').' + '.$index.' days '));
    $harga = 5000 + $index;
    $html3 .= '<tr>
    <td>'.$no.'</td>
    <td>'.$gudangs3['nama'].'</td>
    <td>'.$mat.'</td>
    <td>'.$kat.'</td>
    <td>kode-'.$index.'-'.$kat.'</td>
    <td>'.$tanggalProduksi.'</td>
    <td>'.$harga.'</td>
    <td>'.$indexStock.'</td>
    </tr>';
    $indexStock+=5;
    $no++;
    $index++;
}
}
}
}
$html3.='</tbody></table>';

$html4 ='<table>
  <tr>
    <th>no</th>
    <th>nama gudang</th>
    <th>nama material</th>
    <th>kategori</th>
    <th>kode</th>
    <th>tanggal produksi</th>
    <th>harga satuan</th>
    <th>stock</th>
  </tr><tbody>';
foreach($gudang4 as $gudangs4){
  foreach($gudangs4['kategori'] as $kat){
    $index = 1;
    $indexStock = 5;
foreach($gudangs4['material'] as $mat){
  if($mat == $material || $material == 'all' ){
  $tanggalProduksi = date('d F Y',strtotime(date('Y-m-d').' + '.$index.' days '));
    $harga = 5000 + $index;
    $html4 .= '<tr>
    <td>'.$no.'</td>
    <td>'.$gudangs4['nama'].'</td>
    <td>'.$mat.'</td>
    <td>'.$kat.'</td>
    <td>kode-'.$index.'-'.$kat.'</td>
    <td>'.$tanggalProduksi.'</td>
    <td>'.$harga.'</td>
    <td>'.$indexStock.'</td>
    </tr>';
    $indexStock+=5;
    $no++;
    $index++;
}
}
}
}
$html4.='</tbody></table>';

$html5 ='<table>
  <tr>
    <th>no</th>
    <th>nama gudang</th>
    <th>nama material</th>
    <th>kategori</th>
    <th>kode</th>
    <th>tanggal produksi</th>
    <th>harga satuan</th>
    <th>stock</th>
  </tr>
  <tbody>';
foreach($gudang5 as $gudangs5){
  foreach($gudangs5['kategori'] as $kat){
    $index = 1;
    $indexStock = 5;
foreach($gudangs5['material'] as $mat){
  if($mat == $material || $material == 'all' ){
  $tanggalProduksi = date('d F Y',strtotime(date('Y-m-d').' + '.$index.' days '));
    $harga = 5000 + $index;
    $html5 .= '<tr>
    <td>'.$no.'</td>
    <td>'.$gudangs5['nama'].'</td>
    <td>'.$mat.'</td>
    <td>'.$kat.'</td>
    <td>kode-'.$index.'-'.$kat.'</td>
    <td>'.$tanggalProduksi.'</td>
    <td>'.$harga.'</td>
    <td>'.$indexStock.'</td>
    </tr>';
    $indexStock+=5;
    $no++;
    $index++;
}
}
}
}
$html5.=' </tbody></table>';
$finalHtml = [];
$finalHtml[0]=$html;
$finalHtml[1]=$html2;
$finalHtml[2]=$html3;
$finalHtml[3]=$html4;
$finalHtml[4]=$html5;
return $finalHtml;
  }

  function getDataGudangbyNamaGudang($nama){
    $data = data($nama,'all');
    echo $data[0];
    echo $data[1];
    echo $data[2];
    echo $data[3];
    echo $data[4];
    
  }

  function getDataMaterial($nama){
    $data = data('all',$nama);
    echo $data[0];
    echo $data[1];
    echo $data[2];
    echo $data[3];
    echo $data[4];

  }

  function all(){
    $data = data('all','all');
    echo $data[0];
    echo $data[1];
    echo $data[2];
    echo $data[3];
    echo $data[4];
  }



  all();
getDataGudangbyNamaGudang('operasional');
getDataMaterial("Paving Blok Natural 6 cm");
  ?>
   

</body>
</html>

