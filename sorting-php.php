<?php
$arr1 = [1,2,5,1,2,7,3];
$arr2 = ['nama'=>'Gunawan','umur'=>'26','alamat'=>'Bandar Lampung'];
$arr3 = [
    ['nama'=>'aqil','umur'=>'22','alamat'=>'pasuruan'],
    ['nama'=>'izul','umur'=>'20','alamat'=>'Sidoarjo'],
    ['nama'=>'dimas','umur'=>'21','alamat'=>'malang'],
    ['nama'=>'rifkie','umur'=>'23','alamat'=>'pasuruan'],
    ['nama'=>'farel','umur'=>'22','alamat'=>'madura'],
];

// sort();rsort();asort();ksort();arsort();krsort();
// arsort($arr1);
// arsort($arr2);
// arsort($arr3);

usort($arr3,function($b,$a){return strnatcmp($b['alamat'],$a['alamat']);});

foreach($arr1 as $k=>$v){
    echo $k.' = '.$v. '<br>';
}

echo "<hr>";
foreach($arr2 as $k=>$v){echo $k.' = '.$v. '<br>';}

echo "<hr>";
foreach($arr3 as $k=>$v){
    echo $k.' = ';
    print_r ($v);
    echo  '<br>';
}


?>